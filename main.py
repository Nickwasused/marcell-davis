import asyncio
import os
import psutil
import discord
import time
import random
import sys
import json
import os.path
from discord.ext import commands
from discord.ext.commands import Bot

Client = discord.Client()
bot_prefix= "m!"
client = commands.Bot(command_prefix=bot_prefix)

@client.event
async def on_ready():
    zeit = time.strftime("%H:%M:%S")
    spiel = random.randint(1, 3)
    print("Bot starting!")
    if spiel == 1:
       await client.change_presence(game=discord.Game(name='mit einem Modem!'))
    if spiel == 2:
       await client.change_presence(game=discord.Game(name='am 1&1 Smartpad'))
    if spiel == 3:
       await client.change_presence(game=discord.Game(name='seit 16 Jahren bei 1&1'))
    await client.send_message(discord.Object(id='412584731629060098'), 'Ich wurde Um {} gestartet!'.format(zeit))

@client.event
async def on_message(message):
    if message.content.startswith('m!ping'):
        await client.send_message(message.channel, 'Pong!')

    if message.content.startswith('m!a'):
        if message.author.id == "324945454917746700":
             info = ('Der Source Code wurde von Github nach Bitbucket verschoben. \n Der Server wurde komplett neu aufgesetzt!')
             await client.send_message(discord.Object(id='415914838934683660'), '{}'.format(info))
        pass

    if message.content.startswith('m!update'):
        if message.author.id == "324945454917746700":
            zeit = time.strftime("%H:%M:%S")
            await client.send_message(message.channel, 'starte Update')
            os.system("sudo apt-get update")
            os.system("sudo apt-get upgrade -y")
            await client.send_message(message.channel, 'Starte den Bot neu!')
            await client.send_message(discord.Object(id='427184468487307274'), 'Ich werde jetzt ({}) neugestartet!'.format(zeit))
            os.system("sudo reboot now")
        pass

    if message.content.startswith('m!neustart'):
        if message.author.id == "324945454917746700":
            zeit = time.strftime("%H:%M:%S")
            await client.send_message(message.channel, 'Ich werde Neugestartet!')
            await client.send_message(discord.Object(id='427184468487307274'), 'Ich werde jetzt ({}) neugestartet!'.format(zeit))
            os.system("sudo reboot now")

    if message.content.startswith('m!ram'):
        ram = psutil.virtual_memory()
        await client.send_message(message.channel, '{}'.format(ram))

    if message.content.startswith('m!cpu'):
        if message.author.id == "324945454917746700":
            cpu = psutil.cpu_percent(interval=1)
            await client.send_message(message.channel, 'Cpuusage: {}%'.format(cpu))
        if message.author.id == "331161884210495488":
            cpu = psutil.cpu_percent(interval=1)
            await client.send_message(message.channel, 'Cpuusage: {}%'.format(cpu))

    if message.content.startswith('m!zeit'):
        zeit = time.strftime("%H:%M:%S")
        await client.send_message(message.channel, 'Es ist {}'.format(zeit))

    if message.content.startswith('m!münze'):
        choice = random.randint(1, 2)
        if choice == 1:
            await client.send_message(message.channel, 'Kopf')
        if choice == 2:
            await client.send_message(message.channel, 'Zahl')

    if message.content.startswith('m!bin ich cool?'):
        choice = random.randint(1, 2)
        if choice == 1:
            await client.send_message(message.channel, 'Nein')
        if choice == 2:
            await client.send_message(message.channel, 'Ja')

    if message.content.startswith('m!?'):
        choice = random.randint(1, 3)
        if choice == 1:
            await client.send_message(message.channel, 'Nein')
        if choice == 2:
            await client.send_message(message.channel, 'Ja')
        if choice == 3:
            await client.send_message(message.channel, 'vielleicht')

    if message.content.startswith('m!1&1'):
        choice = random.randint(1, 11)
        if choice == 1:
            await client.send_message(message.channel, 'Wir kommen persönlich vorbei!')
            await client.delete_message(message)
        if choice == 2:
            await client.send_message(message.channel, 'Hallo Marcell D‘Avis')
            await client.delete_message(message)
        if choice == 3:
            await client.send_message(message.channel, 'Hallo Marcell D‘Avis Leiter für Kundenzufriedenheit bei 1&1.')
            await client.delete_message(message)
        if choice == 4:
            await client.send_message(message.channel, 'Wir gehen erst wieder, wenn der Anschluss läuft.')
            await client.delete_message(message)
        if choice == 5:
            await client.send_message(message.channel, 'Altes Modem raus neues Modem rein Startcode eingeben und ihr Modem installiert sich ganz von allein.')
            await client.delete_message(message)
        if choice == 6:
            await client.send_message(message.channel, 'Mein Team und ich sind rund um die Uhr für sie da.')
            await client.delete_message(message)
        if choice == 7:
            await client.send_message(message.channel, 'Damit sie mich erreichen, meine Mailadresse: davis@1und1.de')
            await client.delete_message(message)
        if choice == 8:
            await client.send_message(message.channel, 'Manchmal gibt es Dinge, die kann man am Telefon nicht regeln. Dann reden wir auch nicht lange um den heißen Brei herum, sondern kommen persönlich vorbei.')
            await client.delete_message(message)
        if choice == 9:
            await client.send_message(message.channel, 'Wir bei 1&1 haben richtig was zu feiern!')
            await client.delete_message(message)
        if choice == 10:
            await client.send_message(message.channel, 'Wenn sie mit irgendetwas nicht zufrieden sind, verbessern wirs!')
            await client.delete_message(message)
        if choice == 11:
            await client.send_message(message.channel, 'Die neuste Innovation von 1&1 sehen sie hier: Das 1&1 Smartpad.')
            await client.delete_message(message)

    if message.content.startswith('m!raichu'):
        await client.send_message(message.channel, '@Raichu#5354 ist ein KEK')

    if message.content.startswith('m!meme'):
        meme = random.randint(1, 838)
        await client.send_file(message.channel, 'memes/{}.jpg'.format(meme))

    if message.content.startswith('m!software'):
        await client.send_message(message.channel, 'Ich wurde in Python 3.6.1 programmiert!')

    if message.content.startswith('m!hardware'):
        await client.send_message(message.channel, 'Ich laufe auf Raspberry Pi 3b')

    if message.content.startswith('m!version'):
        await client.send_message(message.channel, 'Version: 0.9.3')

    if message.content.startswith('m!einladungslink'):
        await client.send_message(message.channel, 'https://discordapp.com/oauth2/authorize?client_id=412588912997957632&permissions=3393536&scope=bot')

    if message.content.startswith('m!website'):
        await client.send_message(message.channel, '#----------------------------------------------# \n http://185.239.236.106/marcelldavis.html             \n#----------------------------------------------#')

    if message.content.startswith('m!entwickler'):
        await client.send_message(message.channel, '-------------------------------- \n @Nickwasused#0730 und !Weißbrot Fisch!#2890 \n --------------------------------')

    if message.content.startswith('m!befehle'):
            await client.send_message(message.channel, "Fun: \n 1&1 \n ? \n meme \n \n \n ?: \n münze \n info <@user> \n einladungslink \n zeit \n bug <dein text> \n \n \n Informationen: \n website \n software \n hardware \n entwickler \n bitbucket \n version \n \n \n Entwickler Optionen: \n neustart \n update \n cpu \n ram \n \n \n Musik: \n quit \n play(spielt einen random Remix von Youtube)")

    if message.content.startswith('m!bitbucket'):
            github = discord.Embed(
            title="Bitbucket",
            description='https://bitbucket.org/Nickwasused/marcell-davis/src/master/',
            )

            await client.send_message(message.channel, embed=github)

    if message.content.startswith('m!bug'):
       try:
           bugreport = message.content
           bugserver = message.server
           buguser = message.author
           bugtime = time.strftime("%H:%M:%S")
           bugcpu = psutil.cpu_percent(interval=1)
           bugram = psutil.virtual_memory()
           bugsend = discord.Embed(
           title="BugReport",
           description=bugreport,
           )
           bugsend.add_field(
           name="Server:",
           value=bugserver,
           )
           bugsend.add_field(
           name="User:",
           value=buguser,
           )
           bugsend.add_field(
           name="Uhrzeit:",
           value=bugtime,
           )
           bugsend.add_field(
           name="Cpu:",
           value=bugcpu,
           )
           bugsend.add_field(
           name="Ram:",
           value=bugram,
           )

           await client.send_message(discord.Object(id='412584765485744129'), embed=bugsend)
       except:
            await client.send_message(discord.Object(id='412584765485744129'), 'Fehler mit dem Bugreport')
       finally:
            pass

    if message.content.startswith('m!info'):
        try:
            user = message.mentions[0]
            userjoinedat = str(user.joined_at).split('.', 1)[0]
            usercreatedat = str(user.created_at).split('.', 1)[0]
            userembed = discord.Embed(
                title="Name:",
                description=user.name,
                color=0xe67e22
            )
            userembed.set_author(
                name="User Info"
            )
            userembed.add_field(
                name="Beigetreten am:",
                value=userjoinedat
            )
            userembed.add_field(
                name="Account erstellt am:",
                value=usercreatedat
            )
            userembed.add_field(
                name="Id:",
                value=user.id
            )

            await client.send_message(message.channel, embed=userembed)
        except IndexError:
            await client.send_message(message.channel, "Ich konnte den User nicht finden.")
        except:
            await client.send_message(message.channel, "Fehler")
        finally:
            pass


client.run("NDEyNTgzNjA2NzM3ODI5ODg4.DZkW2Q.n2OH32MFTQ7CLcpWvcHrAfYWj_Q")  #you can steal this Token its only for the Build
